---
title: Template para agregar ADR
description: 
---

A continuacion se define un template que se puede tomar como referencia para poder escribir agregar una ADR a la pagina.
Este template es una referencia y no necesariamente hay que respetarlo siempre, la idea principal es tener un registro de las decisiones tomadas y poder registrar el **por que?** de cada una.

para la creacion se puede ejecutar el siguiente comando:

```bash
$ sh adr-new.sh
```

Esto genera un archivo con el siguiente contenido:

```
# 1. Titulo

Fecha: 2021-12-25

## Estado

[Aceptado,Propuesto,Obsoleto]

## Contexto

Motivo por el que se genera la desicion. Se debe dar un contexto que ayude a entender la desicion con las alternativas estudiadas y las ventajas/desventajas observadas de cada alternativa.

## Desicion

Describe la desicion tomada y la forma en la que se deberia implementar.

## Consecuencias

Define una lista de:
- la dificultdad de implementarla
- la forma en la que afecta a la plataforma
- los riegos que mitiga al introducir el cambio propuesto.

```

## Referencias

- [Homepage of the ADR GitHub organization](https://adr.github.io)
- [Architecture decision record (ADR)](https://github.com/joelparkerhenderson/architecture-decision-record)