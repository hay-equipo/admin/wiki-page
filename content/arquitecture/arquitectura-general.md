---
title: Arquitectura general del proyecto
description: Link a la guia de geekdoc
geekdocNav: true
geekdocAlign: left
geekdocAnchor: false
---


## Version inicial de la arquitectura definida

{{< figure src="arquitectura-general_v1.png" lightbox="lightboxOverlay"  width="80%" >}}


- [source](/arquitecture/arquitectura-general/arquitectura.drawio)