---
title: Obtener turno de una cancha
description: 
geekdocNav: true
geekdocAlign: left
geekdocAnchor: false
---

# Flujo para obtener turno desde la app mobile del usuario


{{< figure src="propuesta_flujo_mobile_cliente.jpeg" lightbox="lightboxOverlay"  width="20%" >}}

El Flujo es el siguiete:

- se inicia con el boton "Reservar"
- se muestra el formulario con los datos de busqueda
- se muestra la lista de turnos disponibles
- se selecciona un turno y se reserva.


# Flujo obtener turno

El siguiente diagrama es un draft de lo que pensamos que seria obtener un turno

{{< figure src="flujo_turno.drawio.png" lightbox="lightboxOverlay"  width="80%" >}}


[source](/flow-diagrams/obtener-turno/flujo_turno.drawio)
