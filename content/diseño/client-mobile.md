---
title: Diseño mobile del cliente
description: 
geekdocNav: true
geekdocAlign: left
geekdocAnchor: false
---

# Propuesta inicial de la interface

La siguiente imagen muestra el menu inicial que tendria la aplicación

{{< figure src="imagen_menu_mobile.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

Se puede ver los siguientes items en el menu:
- complejos 
- mis turnos 
- equipo 
- torneos 
- reservas 
- perfil


{{< figure src="imagen_inicial_mobile.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

En la parte superior muestra una barra para buscar, en la busqueda se tiene en cuenta: la cancha, telefono del cliente o el nombre del cliente.

Muestra la lista principal de funciones: 

- El boton "Aceptar" muestra una lista de reservas aceptadas
- El boton "Rechazar" muestra de reservas rechazadas 
- El boton "Pendientes" muestra una lista de reservas pendientes de aprobar/rechazar. 


{{< figure src="imagen_flujo_mobile.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

Se muestra el flujo de reservas, 
- Al iniciar la aplicación, se muestra el boton para iniciar una reserva.
- Al hacer tab sobre el boton, se muestra un formulario para hacer reservas:
  - Fecha: permite seleccionar la fecha de la reserva
  - Deporte: muestra el deporte que se busca.
  - muestra una lista de opciones para hacer la reserva.
  - al seleccionar una, permite completar la reserva.
  