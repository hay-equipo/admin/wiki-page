---
title: Diseño web del cliente
description: 
geekdocNav: true
geekdocAlign: left
geekdocAnchor: false
---

# Propuesta inicial de la interface

Propuesta de la interface web de la plataforma


Pantalla inicial de la web.

{{< figure src="web-0001.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

Pantalla con los turnos de la cancha

{{< figure src="web-0007.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

Pantalla donde se ven las canchas del predio

{{< figure src="web-0002.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

Pantalla con el formulario para agregar una cancha

{{< figure src="web-0003.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

Pantalla con la lista de empleados

{{< figure src="web-0004.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

Pantalla con el formulario para agregar empleados

{{< figure src="web-0005.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

Pantalla con el historial de reservas

{{< figure src="web-0006.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

