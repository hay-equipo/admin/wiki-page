---
title: Diseño mobile del administrador
description: 
geekdocNav: true
geekdocAlign: left
geekdocAnchor: false
---

# Propuesta inicial de la interface

La siguiente figura muestra 3 interfaces:

- arriba: es el menu de la aplicacion
- abajo izquierda: muestra la pantalla en la que el admin de la cancha puede ver las reservas pendientes, aceptadas. Tambien tiene una barra de busqueda.
- abajo derecha: al seleccionar una reserva pendiente, puede ver los detalles de la misma con 2 botones, uno para aceptar y otro para rechazar.

{{< figure src="mobile-0001.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

- Imagen de arriba: muestra la pantalla inicial y en la parte inferior se ve el boton del medio con el simbolo '+', este permite crear una reserva.
- imagen de abajo: muestra el formulario que se debe completar al iniciar el proceso para crear una reserva.

{{< figure src="mobile-0002.jpeg" lightbox="lightboxOverlay" class="example-image-link" width="20%" >}}

