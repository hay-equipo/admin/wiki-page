## Hay Equipo


En esta página se puede acceder a toda la información relacionada al proyecto para la gestion de turnos en canchas.
La pagina tiene difenretes secciones: 

##  ADR 

[Architecture decision record](https://github.com/joelparkerhenderson/architecture-decision-record) sección que almacena las diferentes desiciones que se tomen, desde la seleccion de tecnologias, lenguaje de programación, etc.

## Guias

Sección con guias para poder realizar tareas o configurar ambientes.

## Arquitectura

Seccion donde se muestran diferentes diagramas con la arquitectura del sistema

## Flujos

Sección con los flujos de negocio.

