#!/bin/bash

date=$(date "+%Y-%m-%d")
filecount=$( ls content/adr/ | wc -l)
filenumber=$((${filecount} + 1))
filename=$(printf "adr-%04d" ${filenumber})
filepath=content/adr/${filename}.md

echo "Create file ${filepath}"

cat > ${filepath} <<EOL
---
title: Tecnologias
description: 
---

# 1. Titulo

Fecha: ${date}

## Estado

[Aceptado,Propuesto,Obsoleto]

## Contexto

Motivo por el que se genera la desicion. Se debe dar un contexto que ayude a entender la desicion con las alternativas estudiadas y las ventajas/desventajas observadas de cada alternativa.

## Desicion

Describe la desicion tomada y la forma en la que se deberia implementar.

## Consecuencias

Define una lista de:
- la dificultdad de implementarla
- la forma en la que afecta a la plataforma
- los riegos que mitiga al introducir el cambio propuesto.
EOL







